import java.time.Duration;
import java.time.LocalTime;

public class BiciRunnable implements Runnable{
    private String nom;
    private LocalTime inici;
    private int distancia;
    private long temps;
    private int recoregut=0;

    public BiciRunnable(String nom, int distancia , LocalTime inici) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }

    public String getNom() {
        return nom;
    }

    public long getTemps() {
        return temps;
    }

    @Override
    public void run() {
        moviment();
    }

    public void moviment(){
        while (distancia!=recoregut){
            recoregut++;
        }
        temps = Duration.between(inici,LocalTime.now()).getNano();
    }

    @Override
    public String toString() {
        return nom+" ha trigat " +temps+" unitats de temps en fer una distància de "+recoregut;
    }
}
