import java.time.LocalTime;

public class ProvaBici {
    static LocalTime inici;
    final static int DISTANCIA= 1000;
    public static void main(String[] args) {
        ProvaBici programa= new ProvaBici();

        programa.provaThread();
        programa.provaRunnable();

    }
    public void provaThread(){
        inici = LocalTime.now();
        BiciThreat bt1 = new BiciThreat("Montse", DISTANCIA, inici);
        BiciThreat bt2 = new BiciThreat("Fran", DISTANCIA, inici);
        BiciThreat bt3 = new BiciThreat("Clara", DISTANCIA, inici);

        bt1.start();
        bt2.start();
        bt3.start();
        /*try {
            bt1.sleep(1000l);
            bt2.sleep(1000l);
            bt3.sleep(1000l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        // Amb el mètode sleep espera el temps indicat no a qua acabi. Amb el mètode join es mostra quan acaba.
        try {
            bt1.join();
            bt2.join();
            bt3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Threat");
        System.out.println(bt1);
        System.out.println(bt2);
        System.out.println(bt3);
        if (bt1.getTemps()<bt2.getTemps()&&bt1.getTemps()<bt3.getTemps()){
            System.out.println("La bici de "+bt1.getNom()+" ha estat la més ràpida");
        }else if (bt2.getTemps()<bt1.getTemps()&&bt2.getTemps()<bt3.getTemps()){
            System.out.println("La bici de "+bt2.getNom()+" ha estat la més ràpida");
        }else if (bt3.getTemps()<bt2.getTemps()&&bt3.getTemps()<bt1.getTemps()){
            System.out.println("La bici de "+bt3.getNom()+" ha estat la més ràpida");
        }else if (bt1.getTemps()==bt2.getTemps()){
            if (bt2.getTemps()==bt3.getTemps()){
                System.out.println("Totes les bicis han estat igual de ràpides.");
            }else {
                System.out.println("Les bicis de "+bt1.getNom()+" i "+bt2.getNom()+" han estat les més ràpides.");
            }
        }else if (bt1.getTemps()==bt3.getTemps()){
            System.out.println("Les bicis de "+bt1.getNom()+" i "+bt3.getNom()+" han estat les més ràpides.");
        }else {
            System.out.println("Les bicis de " + bt2.getNom() + " i " + bt3.getNom() + " han estat les més ràpides.");
        }
    }
    public void provaRunnable(){
        inici = LocalTime.now();
        BiciRunnable br1 = new BiciRunnable("Montse", DISTANCIA, inici);
        BiciRunnable br2 = new BiciRunnable("Fran", DISTANCIA, inici);
        BiciRunnable br3 = new BiciRunnable("Clara", DISTANCIA, inici);
        Thread th1=new Thread(br1);
        Thread th2=new Thread(br2);
        Thread th3=new Thread(br3);
        th1.start();
        th2.start();
        th3.start();
        try {
            th1.join();
            th2.join();
            th3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("\nRunnable");
        System.out.println(br1);
        System.out.println(br2);
        System.out.println(br3);
        if (br1.getTemps()<br2.getTemps()&&br1.getTemps()<br3.getTemps()){
            System.out.println("La bici de "+br1.getNom()+" ha estat la més ràpida");
        }else if (br2.getTemps()<br1.getTemps()&&br2.getTemps()<br3.getTemps()){
            System.out.println("La bici de "+br2.getNom()+" ha estat la més ràpida");
        }else if (br3.getTemps()<br2.getTemps()&&br3.getTemps()<br1.getTemps()){
            System.out.println("La bici de "+br3.getNom()+" ha estat la més ràpida");
        }else if (br1.getTemps()==br2.getTemps()){
            if (br2.getTemps()==br3.getTemps()){
                System.out.println("Totes les bicis han estat igual de ràpides.");
            }else {
                System.out.println("Les bicis de "+br1.getNom()+" i "+br2.getNom()+" han estat les més ràpides.");
            }
        }else if (br1.getTemps()==br3.getTemps()){
            System.out.println("Les bicis de "+br1.getNom()+" i "+br3.getNom()+" han estat les més ràpides.");
        }else {
            System.out.println("Les bicis de " + br2.getNom() + " i " + br3.getNom() + " han estat les més ràpides.");
        }
    }
}
